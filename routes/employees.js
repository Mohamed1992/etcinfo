const express = require('express');
const router = express.Router();
const db = require('../models');

const {validateEmployee} = require('../models/Employee') 

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + file.originalname)
    }
})

var upload = multer({ storage: storage })




/**
 * @swagger
 * components:
 *   schemas:
 *     Employee:
 *       type: object
 *       required:
 *         - name
 *         - experiences
 *         - training
 *         - internships
 *         - image
 *         - CompanyId
 *       properties:
 *         id:
 *           type: number
 *           description: The auto-generated id of the employee
 *         name:
 *           type: string
 *           description: The employee name
 *         experiences:
 *           type: string
 *           description: The employee experiences
 *         training:
 *           type: string
 *           description: The employee training
 *         internships:
 *           type: string
 *           description: The employee internships
 *         image:
 *           type: string
 *           description: The employee image
 *         CompanyId:
 *           type: number
 *           description: The id of the company
 *       example:
 *         id: 5
 *         name: Zied Belthith
 *         experiences: Developpeur web 
 *         training: Developpeur web
 *         internships: Developpeur web
 *         image: uploads/zied123456878.jpg
 *         CompanyId: 1
 *         
 */

 /**
  * @swagger
  * tags:
  *   name: Employees
  *   description: The employees managing API
  */


 /**
 * @swagger
 * /employees/upload:
 *   post:
 *     summary: upload image
 *     tags: [Employees]
 *     requestBody:
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               image:
 *                 type: file
 *     responses:
 *       200:
 *         description: The image was successfully saved
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   pathImage:
 *                     type: string
 *       500:
 *         description: Some server error
 */


  router.post('/upload',upload.array('image'),async(req,res)=>{
    const files = req.files
    let arr=[];
    files.forEach(element => {
    
      arr.push({pathImage:element.path})
 
    })
    return res.send(arr)
 })

/**
 * @swagger
 * /employees:
 *   get:
 *     summary: Returns the list of all the employees with their companies 
 *     tags: [Employees]
 *     responses:
 *       200:
 *         description: The list of the employees
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: boolean
 *                 resultat:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                       name:
 *                         type: string
 *                       experiences:
 *                         type: string
 *                       training:
 *                         type: string
 *                       internships:
 *                         type: string
 *                       image:
 *                         type: string
 *                       createdAt:
 *                         type: string
 *                       updatedAt:
 *                         type: string
 *                       CompanyId:
 *                         type: integer
 *                       Company:
 *                         type: object
 *                         properties:
 *                           id:
 *                             type: integer
 *                           name:
 *                             type: string
 *                           createdAt:
 *                             type: string
 *                           updatedAt:
 *                             type: string
 *                         
 */

router.get('/', (req, res) => 
    db.Employee.findAll({include: [db.Company]})
    .then(employees => res.send({status:true,resultat:employees}))
    .catch(err => res.send({status:false, error:err.message}))
);

/**
 * @swagger
 * /employees/find/{id}:
 *   get:
 *     summary: Get the employee by id
 *     tags: [Employees]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: The employee id
 *     responses:
 *      200:
 *        description: The employee was updated
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                 $ref: '#/components/schemas/Employee'
 *      404:
 *        description: The employee was not found
 *      500:
 *        description: Some error happened
 */
 
router.get("/find/:id", (req, res) => {
  db.Employee.findAll({
    where: {
      id: req.params.id
    }
  }).then(employee => res.send(employee));
});


/**
 * @swagger
 * /employees/add:
 *   post:
 *     summary: Create a new employee
 *     tags: [Employees]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema: 
 *             type: object
 *             properties:
 *                name:
 *                  type: string
 *                experiences:
 *                  type: string
 *                training:
 *                  type: string
 *                internships:
 *                  type: string
 *                image:
 *                  type: string
 *                CompanyId:
 *                  type: integer
 *     responses:
 *       200:
 *         description: The employee was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: boolean
 *                 resultat:
 *                   $ref: '#/components/schemas/Employee'
 *       500:
 *         description: Some server error
 */

router.post('/add', (req, res) => {

  const {error} = validateEmployee(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})

  let  {name, experiences, training, internships, image, CompanyId}  = req.body;
  
  // Insert into table
  db.Employee.create({
    name:name,
    experiences:experiences,
    training:training,
    internships:internships,
    image:image,
    CompanyId:CompanyId
  })
  .then(employee => res.send({status:true, resultat: employee}))
  .catch(err => res.send({status:false, error:err.message}))
  
});


/**
 * @swagger
 * /employees/edit/{id}:
 *  put:
 *    summary: Update the employee by the id
 *    tags: [Employees]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The employee id
 *    requestBody:
 *      required: true
 *      content:
 *         application/json:
 *           schema: 
 *             type: object
 *             properties:
 *                name:
 *                  type: string
 *                experiences:
 *                  type: string
 *                training:
 *                  type: string
 *                internships:
 *                  type: string
 *                image:
 *                  type: string
 *                CompanyId:
 *                  type: integer
 *    responses:
 *      200:
 *        description: The employee was updated
 *        content:
 *          application/json:
 *            schema:
 *               type: object
 *               properties:
 *                  status:
 *                    type: boolean
 *      404:
 *        description: The employee was not found
 *      500:
 *        description: Some error happened
 */
 
router.put("/edit/:id", (req, res) => {

  const {error} = validateEmployee(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})

  db.Employee.update(
    {
      name:req.body.name,
      experiences:req.body.experiences,
      training:req.body.training,
      internships:req.body.internships,
      image:req.body.image,
      CompanyId:req.body.CompanyId
    },
    {
      where: { id: req.params.id }
    }
  ).then(() => res.send({status:true}));
});

/**
 * @swagger
 * /employees/delete/{id}:
 *   delete:
 *     summary: Remove the employee by id
 *     tags: [Employees]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The employee id
 * 
 *     responses:
 *       200:
 *         description: The employee was deleted
 *         content:
 *            application/json:
 *              schema:
 *                 type: object
 *                 properties:
 *                    status:
 *                      type: boolean
 *       404:
 *         description: The employee was not found
 */

 router.delete("/delete/:id", (req, res) => {
  db.Employee.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => res.send("success"));
});


module.exports = router;