const express = require('express');
const router = express.Router();
const db = require('../models');
const {validateCompany} = require('../models/Company') 


/**
 * @swagger
 * components:
 *   schemas:
 *     Company:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         id:
 *           type: number
 *           description: The auto-generated id of the company
 *         name:
 *           type: string
 *           description: The company name
 *       example:
 *         id: 5
 *         name: EtcInfo
 *        
 */

 /**
  * @swagger
  * tags:
  *   name: Companies
  *   description: The companies managing API
  */

/**
 * @swagger
 * /companies:
 *   get:
 *     summary: Returns the list of all the companies with their employees
 *     tags: [Companies]
 *     responses:
 *       200:
 *         description: The list of the companies
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: boolean
 *                 resultat:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                       name:
 *                         type: string
 *                       createdAt:
 *                         type: string
 *                       updatedAt:
 *                         type: string
 *                       Employees:
 *                          type: array
 *                          items:
 *                            type: object
 *                            properties:
 *                              id:
 *                                type: integer
 *                              name:
 *                                type: string
 *                              experiences:
 *                                type: string
 *                              training:
 *                                type: string
 *                              internships:
 *                                type: string
 *                              image:
 *                                type: string
 *                              createdAt:
 *                                type: string
 *                              updatedAt:
 *                                type: string
 *                              CompanyId:
 *                                type: integer
 */

router.get('/', (req, res) => 
    db.Company.findAll({include: [db.Employee]})
    .then(companies => res.send({status:true,resultat:companies}))
    .catch(err => res.send({status:false, error:err.message}))
);

/**
 * @swagger
 * /companies/find/{id}:
 *   get:
 *     summary: Get the company by id
 *     tags: [Companies]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: number
 *         required: true
 *         description: The company id
 *     responses:
 *       200:
 *        description: The company by id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                  type: object
 *                  properties:
 *                    id:
 *                      type: integer
 *                    name:
 *                      type: string
 *                    createdAt:
 *                      type: string
 *                    updatedAt:
 *                      type: string
 *       404:
 *         description: The company was not found
 *       500:
 *         description: Some error happened
 */

router.get("/find/:id", (req, res) => {
  db.Company.findAll({
    where: {
      id: req.params.id
    }
  }).then(company => res.send(company));
});


/**
 * @swagger
 * /companies/add:
 *   post:
 *     summary: Create a new company
 *     tags: [Companies]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *     responses:
 *       200:
 *         description: The company was successfully created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status:
 *                   type: boolean
 *                 resultat:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     name:
 *                       type: string
 *                     createdAt:
 *                       type: string
 *                     updatedAt:
 *                       type: string
 *       500:
 *         description: Some server error
 */
router.post('/add', (req, res) => {

  const {error} = validateCompany(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})

  let  name  = req.body.name;

  // Insert into table
  db.Company.create({
    name:name
  })
  .then(company => res.send({status:true, resultat: company}))
  .catch(err => res.send({status:false, error:err.message}))
  
});


/**
 * @swagger
 * /companies/edit/{id}:
 *  put:
 *    summary: Update the company by the id
 *    tags: [Companies]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The company id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *               name:
 *                 type: string
 *    responses:
 *      200:
 *        description: The company was updated
 *        content:
 *          application/json:
 *            schema:
 *               type: object
 *               properties:
 *                  status:
 *                    type: boolean
 *      404:
 *        description: The company was not found
 *      500:
 *        description: Some error happened
 */
router.put("/edit/:id", (req, res) => {
  
  const {error} = validateCompany(req.body)
  if(error) return res.status(400).send({status:false,message:error.details[0].message})

  db.Company.update(
    {
      name: req.body.name
    },
    {
      where: { id: req.params.id }
    }
  ).then(() => res.send({status:true}));
});

/**
 * @swagger
 * /companies/delete/{id}:
 *   delete:
 *     summary: Remove the company by id
 *     tags: [Companies]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The company id
 * 
 *     responses:
 *       200:
 *         description: The company was deleted
 *         content:
 *          application/json:
 *            schema:
 *               type: object
 *               properties:
 *                  status:
 *                    type: boolean
 *       404:
 *         description: The company was not found
 *       500:
 *         description: Some error happened
 */
 router.delete("/delete/:id", (req, res) => {
  db.Company.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => res.send({status:true}));
});


module.exports = router;