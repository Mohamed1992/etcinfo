const Joi=require('joi')

module.exports = (sequelize, DataTypes) => {

const Company = sequelize.define("Company", {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

Company.associate = models => {
  Company.hasMany(models.Employee, {
    onDelete: "cascade"
  });
};

return Company;

};


function validateCompany(request){
    
  const schema=Joi.object({
      name:Joi.string().allow('', null)
  })

  return schema.validate(request)

}

module.exports.validateCompany=validateCompany