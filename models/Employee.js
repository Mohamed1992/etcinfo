const Joi=require('joi')

module.exports = (sequelize, DataTypes) =>{

const Employee = sequelize.define("Employee", {
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  experiences: {
    type: DataTypes.STRING,
    allowNull: false
  },
  training: {
    type: DataTypes.STRING,
    allowNull: false
  },
  internships: {
    type: DataTypes.STRING,
    allowNull: false
  },
  image: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

Employee.associate = models => {
    Employee.belongsTo(models.Company, {
        foreignKey:{  
          allowNull: false
        }
    });
};


return Employee;

};


function validateEmployee(request){
    
  const schema=Joi.object({
      name:Joi.string().allow('', null),
      experiences:Joi.string().allow('', null),
      training:Joi.string().allow('', null),
      internships:Joi.string().allow('', null),
      image:Joi.string().allow('', null),
      CompanyId:Joi.number().required()
  })

  return schema.validate(request)

}

module.exports.validateEmployee=validateEmployee
